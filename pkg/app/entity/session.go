package entity

//go:generate easyjson session.go

// Session store user session
type Session struct {
	Token string
	Value SessionValue
}

// SessionValue store value of session
// easyjson:json
type SessionValue struct {
	UserID   string
	TokenFCM string
}

// MarshalBinary marshal session value into json
func (v SessionValue) MarshalBinary() ([]byte, error) {
	return v.MarshalJSON()
}

// UnmarshalBinary unmarshal session value from json
func (v *SessionValue) UnmarshalBinary(data []byte) error {
	return v.UnmarshalJSON(data)
}
