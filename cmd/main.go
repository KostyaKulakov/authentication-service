package main

import (
	"flag"
	"net"

	"github.com/sirupsen/logrus"
	"gitlab.com/ZorinArsenij/authentication-service/internal/redis"
	"gitlab.com/ZorinArsenij/authentication-service/internal/wire"
	"gitlab.com/ZorinArsenij/authentication-service/pkg/app/delivery/grpc/session"
	"google.golang.org/grpc"
)

func main() {
	addr := flag.String("addr", "127.0.0.1:8081", "service address")
	redisAddr := flag.String("redisAddr", "127.0.0.1:6379", "redis address")
	redisPass := flag.String("redisPass", "", "redis password")
	flag.Parse()

	log := logrus.New()

	conn, err := redis.NewConnection(*redisAddr, *redisPass)
	if err != nil {
		log.Fatalf("connection to redis failed: %s", err)
	}

	server := grpc.NewServer(grpc.UnaryInterceptor(session.UnaryLogInterceptor(log)))
	session.RegisterSessionServer(server, wire.InitializeService(conn))

	lis, err := net.Listen("tcp", *addr)
	if err != nil {
		log.Fatal("failed to listen address", *addr)
	}

	log.Println(server.Serve(lis))
}
