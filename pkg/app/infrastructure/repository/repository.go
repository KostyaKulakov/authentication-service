package repository

import (
	"time"

	"github.com/go-redis/redis"
	"gitlab.com/ZorinArsenij/authentication-service/pkg/errors"
)

const (
	expiration = 6 * time.Hour
)

// Repository stores tarantool connection
type Repository struct {
	conn *redis.Client
}

// New create new instance of tarantool repository
func New(conn *redis.Client) Repository {
	return Repository{
		conn: conn,
	}
}

// Insert add new record to database
func (r Repository) Insert(key string, value interface{}) error {
	return r.conn.Set(key, value, expiration).Err()
}

// Get receive record from database
func (r Repository) Get(key string, value interface{}) error {
	err := r.conn.Get(key).Scan(value)
	if err != nil {
		if err == redis.Nil {
			return errors.ErrNotFound
		}

		return errors.ErrInconsistentData
	}

	return nil
}

// Delete remove tuple from space
func (r Repository) Delete(key string) error {
	return r.conn.Del(key).Err()
}
