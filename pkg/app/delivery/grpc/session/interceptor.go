package session

import (
	context "context"
	"time"

	"github.com/sirupsen/logrus"
	grpc "google.golang.org/grpc"
	status "google.golang.org/grpc/status"
)

type contextKey string

const (
	LoggerKey contextKey = "logger"
)

// UnaryLogInterceptor returns a new unary server interceptors that adds logrus.Entry to the context
func UnaryLogInterceptor(log *logrus.Logger) grpc.UnaryServerInterceptor {
	return func(ctx context.Context,
		req interface{},
		info *grpc.UnaryServerInfo,
		handler grpc.UnaryHandler) (interface{}, error) {
		ctx = context.WithValue(ctx, LoggerKey, log)
		start := time.Now()

		// Calls the handler
		resp, err := handler(ctx, req)
		fields := logrus.Fields{
			"status": status.Code(err).String(),
			"time":   time.Since(start),
			"method": info.FullMethod,
		}

		log.WithFields(fields).Info()
		return resp, err
	}
}
