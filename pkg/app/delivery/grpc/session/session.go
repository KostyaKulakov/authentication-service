package session

import (
	context "context"

	"gitlab.com/ZorinArsenij/authentication-service/pkg/app/entity"
	"gitlab.com/ZorinArsenij/authentication-service/pkg/errors"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
)

//go:generate protoc --go_out=plugins=grpc:. session.proto

type SessionUsecase interface {
	Create(userID string, tokenFCM string) (*entity.Session, error)
	Get(token string) (*entity.Session, error)
	Delete(token string) error
}

// SessionService grpc service for working with sessions
type SessionService struct {
	usecase SessionUsecase
}

// NewSessionService create instance of SessionService
func NewSessionService(usecase SessionUsecase) SessionService {
	return SessionService{
		usecase: usecase,
	}
}

// Get find session
func (s SessionService) Get(ctx context.Context, request *SessionRequest) (*SessionResponse, error) {
	session, err := s.usecase.Get(request.Token)
	switch err {
	case nil:
		return &SessionResponse{
			Token:    session.Token,
			UserID:   session.Value.UserID,
			TokenFCM: session.Value.TokenFCM,
		}, nil
	case errors.ErrNotFound:
		st := status.New(codes.NotFound, err.Error())
		return &SessionResponse{}, st.Err()
	case errors.ErrInconsistentData:
		st := status.New(codes.NotFound, err.Error())
		return &SessionResponse{}, st.Err()
	default:
		return &SessionResponse{}, err
	}
}

// Create set up new session
func (s SessionService) Create(ctx context.Context, request *SessionCreateRequest) (*SessionResponse, error) {
	session, err := s.usecase.Create(request.UserID, request.TokenFCM)
	if err != nil {
		return &SessionResponse{}, err
	}

	return &SessionResponse{
		Token:    session.Token,
		UserID:   session.Value.UserID,
		TokenFCM: session.Value.TokenFCM,
	}, nil
}

// Delete remove session
func (s SessionService) Delete(ctx context.Context, request *SessionRequest) (*Nothing, error) {
	err := s.usecase.Delete(request.Token)
	return &Nothing{}, err
}
