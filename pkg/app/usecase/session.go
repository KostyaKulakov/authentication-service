package usecase

import (
	uuid "github.com/satori/go.uuid"
	"gitlab.com/ZorinArsenij/authentication-service/pkg/app/entity"
)

type SessionRepository interface {
	Insert(key string, value interface{}) error
	Get(key string, value interface{}) error
	Delete(key string) error
}

// SessionUsecase process business logic of sessions
type SessionUsecase struct {
	repo SessionRepository
}

// NewSessionUsecase create instance of SessionUsecase
func NewSessionUsecase(repo SessionRepository) SessionUsecase {
	return SessionUsecase{
		repo: repo,
	}
}

// Create set up new user session
func (s SessionUsecase) Create(userID string, tokenFCM string) (*entity.Session, error) {
	token := uuid.NewV4().String()
	value := entity.SessionValue{
		UserID:   userID,
		TokenFCM: tokenFCM,
	}

	if err := s.repo.Insert(token, value); err != nil {
		return nil, err
	}

	return &entity.Session{
		Token: token,
		Value: entity.SessionValue{
			UserID:   userID,
			TokenFCM: tokenFCM,
		},
	}, nil
}

// Get find session information by token
func (s SessionUsecase) Get(token string) (*entity.Session, error) {
	var value entity.SessionValue
	if err := s.repo.Get(token, &value); err != nil {
		return nil, err
	}

	return &entity.Session{
		Token: token,
		Value: value,
	}, nil
}

// Delete remove session
func (s SessionUsecase) Delete(token string) error {
	return s.repo.Delete(token)
}
